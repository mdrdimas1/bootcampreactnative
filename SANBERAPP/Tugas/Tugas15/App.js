import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createDrawerNavigator } from '@react-navigation/drawer';   
import { Login, About, AddScreen, Skill, Project } from './Screen/export'

const Drawer = createDrawerNavigator();
const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();


function LoginButton() {
    return (
        <Drawer.Navigator initialRouteName="Home">
            <Drawer.Screen name="Home" component={HomeScreen} />
            <Drawer.Screen name="About" component={About} />
        </Drawer.Navigator>
    );
}

function HomeScreen() {
    return (
        <Tab.Navigator>
            <Tab.Screen name="Test1" component={Skill} />
            <Tab.Screen name="Project" component={Project} />
            <Tab.Screen name="Add" component={AddScreen} />
        </Tab.Navigator>
    );
}

function App() {
    return (
        <NavigationContainer>
            <Stack.Navigator initialRouteName="Login" screenOptions={{ headerShown: false }}>
                <Stack.Screen name="Login" component={Login} />
                <Stack.Screen name="HomeScreen" component={LoginButton} />
            </Stack.Navigator>
        </NavigationContainer>
    );
}

export default App;