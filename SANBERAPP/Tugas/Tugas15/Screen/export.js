import Login from './LoginScreen';
import About from './AboutScreen';
import AddScreen from './AddScreen';
import Skill from './SkillScreen';
import Project from './ProjectScreen';

export {Login, About, AddScreen, Skill, Project};