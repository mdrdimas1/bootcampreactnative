import React, { Component } from 'react'
import { Text, StyleSheet, View, Image, TextInput, TouchableOpacity } from 'react-native'

export default class login extends Component {
    render() {
        return (
            <View style={styles.container}>
            {/* logo */}
                <View style={styles.containerLogo}>
                    <Image style={styles.logo}
                        source={require('../../images/Logo_PP.png')} />
                    <Text style={styles.textLogo}>Login</Text>
                </View>

            {/* text input */}
                <View style={styles.form}>
                    <Text style={styles.formText}>Username / Email</Text>
                    <TextInput 
                        style={styles.inputStyle}
                    ></TextInput>
                    
                    <Text style={styles.formText}>Password</Text>
                    <TextInput
                        style={styles.inputStyle}
                        secureTextEntry={true}
                    ></TextInput>
                </View>
            
            {/* tombol */}
                <View style={styles.containerTombol} >
                    <TouchableOpacity style={styles.tombol} >
                        <Text style={styles.tombolText}>Masuk</Text>
                    </TouchableOpacity>
                        
                        <Text style={styles.or}>Atau    </Text>

                    <TouchableOpacity style={styles.tombol2} >
                        <Text style={styles.tombolText}>Daftar</Text>
                    </TouchableOpacity>
                </View>

            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'
    },
    containerLogo: {
        alignItems: 'center',
        marginTop: 50
    },
    logo: {
        width: 375,
        height: 102
    },
    textLogo: {
        marginTop: 60,
        fontSize: 24,
        fontWeight: 'bold'
    },
    form: {
        marginHorizontal: 30,
        marginTop: 50
    },
    inputStyle: {
        color: 'black',
        width: '100%',
        marginTop: 8,
        borderWidth: 1,
        height: 48,
        padding: 10,
        borderRadius: 10,
        marginBottom: 20
    },
    formText: {
        fontSize: 15
    },
    containerTombol: {
        alignItems: 'center'
    },
    tombol: {
        backgroundColor: '#3EC6FF',
        paddingVertical: 15,
        paddingHorizontal: 50,
        borderRadius: 50,
        marginTop: 5
    },
    tombolText: {
        fontSize: 18,
        color: 'white'
    },
    tombol2: {
        backgroundColor: '#003366',
        paddingVertical: 15,
        paddingHorizontal: 50,
        borderRadius: 50
    },
    or: {
        fontSize: 18,
        color: '#3EC6FF',
        marginVertical: 15
    }

})
