import React, { Component } from 'react'
import { Text, StyleSheet, View, ScrollView } from 'react-native'
import { Ionicons, MaterialCommunityIcons, AntDesign } from '@expo/vector-icons';

export default class about extends Component {
    render() {
        return (
            <ScrollView>
                <View style={styles.container}>
                    <View style={styles.container2} >
                        <Text style={styles.about}>Tentang Saya</Text>

                        <View style={styles.containerPerson}>
                            <Ionicons name="person"
                                size={135}
                                color={'#CACACA'} />
                        </View>

                        <Text style={styles.label}>Mukhlis Hanafi</Text>
                        <Text style={styles.label2}>React Native Developer</Text>

                        <View style={styles.container3}>

                            <View style={styles.Judul}>
                                <Text style={styles.labelJudul}>Portofolio</Text>
                            </View>

                            <View style={styles.potofolio}>
                                <View style={styles.containerPotofolio}>
                                    <MaterialCommunityIcons name="gitlab" size={42} color="#3EC6FF" />
                                    <Text style={styles.labelPotofolio}>@mukhlish</Text>
                                </View>
                                <View style={styles.containerPotofolio}>
                                    <AntDesign name="github" size={42} color="#3EC6FF" />
                                    <Text style={styles.labelPotofolio}>@mukhlish</Text>
                                </View>
                            </View>

                        </View>

                        <View style={styles.container3}>

                            <View style={styles.Judul}>
                                <Text style={styles.labelJudul}>Hubungi Saya</Text>
                            </View>

                            <View style={styles.potofolio2}>
                                <View style={styles.containerPotofolio2}>
                                    <Ionicons name="ios-logo-facebook" size={42} color="#3EC6FF" />
                                    <Text style={styles.labelPotofolio2}>mukhlish.hanafi</Text>
                                </View>

                                <View style={styles.containerPotofolio2}>
                                    <Ionicons name="ios-logo-instagram" size={42} color="#3EC6FF" />
                                    <Text style={styles.labelPotofolio2}>@mukhlish.hanafi</Text>
                                </View>

                                <View style={styles.containerPotofolio2}>
                                    <Ionicons name="ios-logo-twitter" size={42} color="#3EC6FF" />
                                    <Text style={styles.labelPotofolio2}>@mukhlish</Text>
                                </View>
                            </View>
                            
                        </View>

                        <View style={{marginBottom: 100}}/>

                    </View>
                </View>
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    container2: {
        alignItems: 'center',
        marginTop: 65
    },
    about: {
        fontSize: 35,
        fontWeight: '700',
        color: '#003366'
    },
    containerPerson: {
        backgroundColor: '#EFEFEF',
        borderRadius: 100,
        width:200,
        height: 200,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 40
    },
    label: {
        marginTop: 20,
        fontSize: 23,
        color: '#003366',
        fontWeight: '700'
    },
    label2: {
        marginTop: 5,
        fontSize: 16,
        color: '#3EC6FF',
        fontWeight: '700'
    },
    container3: {
        marginTop: 16,
        backgroundColor: '#EFEFEF',
        width: '90%',
        borderRadius: 25,
        paddingHorizontal: 15,
        paddingVertical: 10,
    },
    Judul: {
        borderBottomWidth: 1,
        paddingBottom: 6,
        borderColor: '#003366'
    },
    labelJudul: {
        fontSize: 18,
        color: '#003366',
        fontWeight: '400'
    },
    potofolio: {
        alignItems: 'center',
        justifyContent: 'space-around',
        flexDirection: 'row',
        marginVertical: 19
    },
    labelPotofolio: {
        fontWeight: '700',
        fontSize: 16,
        color: '#003366',
        marginTop: 10
    },
    containerPotofolio: {
        flexDirection: 'column',
        alignItems: 'center',
    },
    potofolio2: {
        alignItems: 'center',
        justifyContent: 'space-around',
        flexDirection: 'column',
        marginVertical: 19,
    },
    containerPotofolio2: {
        flexDirection: 'row',
        alignItems: 'center',
        marginVertical: 15,
        justifyContent: 'space-evenly'
    },
    labelPotofolio2: {
        fontWeight: '700',
        fontSize: 16,
        color: '#003366',
        marginLeft: 10
    },
})
 