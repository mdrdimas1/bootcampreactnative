import React, { Component } from 'react'
import { Text, StyleSheet, View,Image, TouchableOpacity,FlatList } from 'react-native'
import { MaterialIcons,FontAwesome,Entypo  } from '@expo/vector-icons';
import VideoItem from './Tugas/Tugas12/components';
import data from './Tugas/Tugas12/data.json'

export default class App extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.navBar}>
          <Image style={styles.logo} 
          source={require('./Tugas/Tugas12/images/logo.png')} />
          <View style={styles.rightNav}> 
          <TouchableOpacity>
            <MaterialIcons style={styles.navItem} name="search" size={25} />
          </TouchableOpacity>
          <TouchableOpacity>
            <MaterialIcons style={styles.navItem} name="account-circle" size={25} color="black" />
          </TouchableOpacity>
          </View>
        </View>
        <View style={styles.body}>
          <FlatList
          data={data.items}
          renderItem={(video)=><VideoItem video={video.item} />}
          keyExtractor={(item)=>item.id}
          ItemSeparatorComponent={()=><View style={{height:0.5,backgroundColor:'#E5E5E5'}}/>}

           />
        </View>
        <View style={styles.tabBar}>

            <TouchableOpacity style={styles.tabItem}>
              <FontAwesome name="home" size={24} />
              <Text style={styles.tabTitle} >Home</Text>
            </TouchableOpacity>

            <TouchableOpacity style={styles.tabItem}>
              <MaterialIcons name="whatshot" size={24} />
              <Text style={styles.tabTitle} >Trending</Text>
            </TouchableOpacity>

            <TouchableOpacity style={styles.tabItem}>
              <MaterialIcons name="subscriptions" size={24} />
              <Text style={styles.tabTitle} >Subscriptions</Text>
            </TouchableOpacity>

            <TouchableOpacity style={styles.tabItem}>
              <Entypo name="folder" size={24} color="black" />
              <Text style={styles.tabTitle} >Library</Text>
            </TouchableOpacity>
          
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  navBar: {
    height: 55, 
    backgroundColor: 'white',
    elevation: 3,
    paddingHorizontal: 15,
    flexDirection: 'row',
    marginTop: 15,
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  logo: {
    width: 98,
    height: 22
  },
  rightNav: {
    flexDirection: 'row'
  },
  navItem: {
    marginLeft: 15
  },
  body: {
    flex: 1
  },
  tabBar: {
    backgroundColor: 'white',
    height: 60,
    borderTopWidth: 0.5,
    borderColor: '#E5E5E5',
    justifyContent: 'space-around',
    flexDirection: 'row'  
  },
  tabItem: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  tabTitle: {
    fontSize: 11,
    color: '#3C3C3C',
    paddingTop: 4
  }
})
