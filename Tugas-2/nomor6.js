var hari = 21; 
var bulan = 12; 
var tahun = 1945;

switch (bulan) {
    case 1:
        console.log("21 Januari 1945");
        break;
    
    case 2:
        console.log("21 Febuari 1945");
        break;
    
    case 3:
        console.log("21 Maret 1945");
        break;

    case 4:
        console.log("21 April 1945");
        break;

    case 5:
        console.log("21 Juni 1945");
        break;

    case 6:
        console.log("21 Juli 1945");
        break;

    default:
        console.log("Masukan bulan 1 sampai 6 saja");
        break;
}