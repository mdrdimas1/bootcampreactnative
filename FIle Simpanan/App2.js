import React from 'react'
import { StyleSheet, Text, View, Button } from 'react-native'
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { CreateAccount, Home, Profile, SignIn } from './Screen';

const AuthStack = createStackNavigator();
const Tab = createBottomTabNavigator();

const ScreenContainer = ({ children }) => (
    <View style={styles.container}>{children}</View>
);

function HomeScreen() {
    return (
        <Tab.Navigator>
            <Tab.Screen name="Home" component={HOMEE} />
            <Tab.Screen name="Home2" component={HOMEE2} />
        </Tab.Navigator>
    );
}

const HOMEE = () => (
    <ScreenContainer>
        <Text>Search2 Screen</Text>
    </ScreenContainer>
)

const HOMEE2 = () => (
    <ScreenContainer>
        <Text>Search222 Screen</Text>
    </ScreenContainer>
)

function LoginScreen({ navigation }) {
    return (
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            <Button
                onPress={() => navigation.navigate('Notifications')}
                title="Go to notifications"
            />
        </View>
    );
}


function NotificationsScreen({ navigation }) {
    return (
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            <Button onPress={() => navigation.goBack()} title="Go back home" />
        </View>
    );
}

const Drawer = createDrawerNavigator();

export default function Appss() {
    return (
        <NavigationContainer>
            <Drawer.Navigator initialRouteName="Home">
                <Drawer.Screen name="Home" component={HomeScreen} />
                <Drawer.Screen name="Login" component={LoginScreen} />
                <Drawer.Screen name="Notifications" component={NotificationsScreen} />
            </Drawer.Navigator>
        </NavigationContainer>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center"
    },
    button: {
        paddingHorizontal: 20,
        paddingVertical: 10,
        marginVertical: 10,
        borderRadius: 5
    }
});