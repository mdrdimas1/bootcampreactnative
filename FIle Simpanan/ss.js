import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { CreateAccount, Home, Profile, SignIn } from './Screen';

const AuthStack = createStackNavigator();
const Tab = createBottomTabNavigator();
const HomeStack = createStackNavigator();
const ProfileStack = createStackNavigator();
const Drawer = createDrawerNavigator();

const HomeStackScreen = () => (
    <HomeStack.Navigator>
        <HomeStack.Screen name="Home" component={Home} />
    </HomeStack.Navigator>
)

const HOMEE = () => (
    <ScreenContainer>
        <Text>Search2 Screen</Text>
    </ScreenContainer>
)

const ProfileStackScreen = () => (
    <HomeStack.Navigator>
        <HomeStack.Screen name="Profile" component={Profile} />
    </HomeStack.Navigator>
)

function HomeScreen({ navigation }) {
    return (
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            <Button
                onPress={() => navigation.navigate('Notifications')}
                title="Go to notifications"
            />
        </View>
    );
}



export default function App() {
    return (
        <NavigationContainer>
            {/* <AuthStack.Navigator>
                <AuthStack.Screen name="SignIn" component={SignIn} />
                <AuthStack.Screen name="CreateAccount" component={CreateAccount} />
            </AuthStack.Navigator> */}
            <Tab.Navigator>
                <Drawer.Navigator initialRouteName="Home">
                    <Drawer.Screen name="Home" component={HomeScreen} />
                </Drawer.Navigator>
                {/* <Tab.Screen name="Home" component={HomeStackScreen} />
                <Tab.Screen name="Profile" component={ProfileStackScreen} /> */}
            </Tab.Navigator>
        </NavigationContainer>
    )
}

const styles = StyleSheet.create({})
