var readBooksPromise = require('./promise.js')

var books = [
    { name: 'LOTR', timeSpent: 3000 },
    { name: 'Fidas', timeSpent: 2000 },
    { name: 'Kalkulus', timeSpent: 4000 }
]

// Lanjutkan code untuk menjalankan function readBooksPromise
let sisaWaktu = 10000
let i = 0
readBooksPromise(sisaWaktu, books[i])
    .then(function (fulfilled) {
        i++
        if(i<books.length){
            readBooksPromise(sisaWaktu, books[i])
        }
    }).catch(function (error) {
        console.log(error)
    })
