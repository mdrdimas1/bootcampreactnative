// No.5
function balikKata(param) {
    let reverseWord = ``;
    for (let i = param.length; i > 0; i--) {
      reverseWord += `${param[i - 1]}`;
    }
    return reverseWord;
  }
  console.log(balikKata("Kasur Rusak")); // kasuR rusaK
  console.log(balikKata("SanberCode")); // edoCrebnaS
  console.log(balikKata("Haji Ijah")); // hajI ijaH
  console.log(balikKata("racecar")); // racecar
  console.log(balikKata("I am Sanbers")); // srebnaS ma I
  
  