// No.1
function range(startNum, finishNum) {
    let arr = [];
    if (startNum && finishNum) {
      if (startNum > finishNum) {
        [startNum, finishNum] = [finishNum, startNum];
        for (let i = startNum; i <= finishNum; i++) {
          arr.push(i);
        }
        arr = arr.sort(function (value1, value2) {
          return value2 - value1;
        });
      } else {
        for (let i = startNum; i <= finishNum; i++) {
          arr.push(i);
        }
      }
      return arr;
    } else {
      return -1;
    }
  }
  console.log(range(1, 10)); //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
  console.log(range(1)); // -1
  console.log(range(11, 18)); // [11, 12, 13, 14, 15, 16, 17, 18]
  console.log(range(54, 50)); // [54, 53, 52, 51, 50]
  console.log(range()); // -1
  