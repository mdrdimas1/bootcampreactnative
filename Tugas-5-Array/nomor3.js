// No.3
function sum(startNum, finishNum, step) {
    if (startNum || finishNum) {
      let arr = [];
      finishNum = finishNum ? finishNum : startNum;
      let total = 0;
      step = step ? step : 1;
      if (startNum > finishNum) {
        for (let i = startNum; i >= finishNum; i = i - step) {
          arr.push(i);
          total += i;
        }
      } else {
        for (let i = startNum; i <= finishNum; i = i + step) {
          arr.push(i);
          total += i;
        }
      }
      return total;
    } else {
      return 0;
    }
  }
  console.log(sum(1, 10)); // 55
  console.log(sum(5, 50, 2)); // 621
  console.log(sum(15, 10)); // 75
  console.log(sum(20, 10, 2)); // 90
  console.log(sum(1)); // 1
  console.log(sum()); // 0
  
 