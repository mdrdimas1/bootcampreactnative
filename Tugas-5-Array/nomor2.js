
  // No.2
  function rangeWithStep(startNum, finishNum, step) {
    if (startNum && finishNum) {
      let arr = [];
      if (startNum > finishNum) {
        [startNum, finishNum] = [finishNum, startNum];
        for (let i = startNum; i <= finishNum; i = i + step) {
          arr.push(i);
        }
        arr = arr.sort(function (value1, value2) {
          return value2 - value1;
        });
      } else {
        for (let i = startNum; i <= finishNum; i = i + step) {
          arr.push(i);
        }
      }
  
      return arr;
    } else {
      return -1;
    }
  }
  console.log(rangeWithStep(1, 10, 2)); // [1, 3, 5, 7, 9]
  console.log(rangeWithStep(11, 23, 3)); // [11, 14, 17, 20, 23]
  console.log(rangeWithStep(5, 2, 1)); // [5, 4, 3, 2]
  console.log(rangeWithStep(29, 2, 4)); // [29, 25, 21, 17, 13, 9, 5]
  
  