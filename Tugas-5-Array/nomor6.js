// No.6
var input = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"];
input.splice(1, 2, "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung");
input.splice(4, 1, "Pria", "SMA Internasional Metro");
console.log(input);

let splitBulan = input[3].split("/");
switch (splitBulan[1]) {
  case "01": {
    bulan = "Januari";
    break;
  }
  case "02": {
    bulan = "Februari";
    break;
  }
  case "03": {
    bulan = "Maret";
    break;
  }
  case "04": {
    bulan = "April";
    break;
  }
  case "05": {
    bulan = "Mei";
    break;
  }
  case "06": {
    bulan = "Juni";
    break;
  }
  case "07": {
    bulan = "Juli";
    break;
  }
  case "08": {
    bulan = "Agustus";
    break;
  }
  case "09": {
    bulan = "September";
    break;
  }
  case "10": {
    bulan = "Oktober";
    break;
  }
  case "11": {
    bulan = "November";
    break;
  }
  case "12": {
    bulan = "Desember";
    break;
  }
}
console.log(bulan);
let desc = [...splitBulan];
desc.sort(function (value1, value2) {
  return value2 - value1;
});
console.log(desc);

console.log(splitBulan.join("-"));
console.log(input[1].slice(0, 15));
