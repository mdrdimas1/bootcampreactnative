function shoppingTime(memberId, money) {
    // you can only write your code here!
    console.log('')
    var harga = {
        sepatu_stacattu: 1500000,
        baju_zoro: 500000,
        baju_hn: 250000,
        sweater_uniklooh: 175000,
        casing_hp: 50000
    }
    var listPurchased = []
    var output = {}
    if (memberId === undefined || memberId === '') {
        return 'Mohon maaf, toko X hanya berlaku untuk member saja'
    } else if (money < 50000) {
        return 'Mohon maaf, uang tidak cukup'
    }
    output.memberId = memberId
    output.money = money
    while (money >= harga.casing_hp) {
        if (money >= harga.sepatu_stacattu) {
            if (!listPurchased.includes('Sepatu Stacattu')) {
                listPurchased.push('Sepatu Stacattu')
                money -= harga.sepatu_stacattu
            }
        } else if (money >= harga.baju_zoro) {
            if (!listPurchased.includes('Baju Zoro')) {
                listPurchased.push('Baju Zoro')
                money -= harga.baju_zoro
            }
        } else if (money >= harga.baju_hn) {
            if (!listPurchased.includes('Baju H&N')) {
                listPurchased.push('Baju H&N')
                money -= harga.baju_hn
            }
        } else if (money >= harga.sweater_uniklooh) {
            if (!listPurchased.includes('Sweater Uniklooh')) {
                listPurchased.push('Sweater Uniklooh')
                money -= harga.sweater_uniklooh
            }
        } else if (money >= harga.casing_hp) {
            if (!listPurchased.includes('Casing Handphone')) {
                listPurchased.push('Casing Handphone')
                money -= harga.casing_hp
            } else {
                break;
            }
        }
    }
    output.listPurchased = listPurchased
    output.changeMoney = money
    return output
}

// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
//{ memberId: '1820RzKrnWn08',
// money: 2475000,
// listPurchased:
//  [ 'Sepatu Stacattu',
//    'Baju Zoro',
//    'Baju H&N',
//    'Sweater Uniklooh',
//    'Casing Handphone' ],
// changeMoney: 0 }
console.log(shoppingTime('82Ku8Ma742', 170000));
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

