//Soal Nomer 1
class Animal {
    constructor(name, legs = 4, cold_blooded = false) {
        this.name = name
        this.legs = legs
        this.cold_blooded = cold_blooded
    }
}

var sheep = new Animal("shaun");
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

class Ape extends Animal {
    constructor(name, legs = 2, cold_blooded = false) {
        super(name);
        this.legs = legs
        this.cold_blooded = cold_blooded
    }
    yell() {
        console.log("Auooo");
    }
}

class Frog extends Animal {
    constructor(name, legs = 4, cold_blooded = true) {
        super(name);
        this.legs = legs
        this.cold_blooded = cold_blooded
    }
    jump() {
        console.log("hop hop");
    }
}

var sungokong = new Ape("kera sakti")
sungokong.yell() // "Auooo"

var kodok = new Frog("buduk")
kodok.jump() // "hop hop" 

//Class Nomer 2
class Clock {
    constructor(template) {
        this.template = template.template;
        this.timer;
    }

    render() {
        var date = new Date();
        var template = this.template;

        var hours = date.getHours();
        if (hours < 10) hours = '0' + hours;

        var mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins;

        var secs = date.getSeconds();
        if (secs < 10) secs = '0' + secs;

        var output = template.replace('h', hours).replace('m', mins).replace('s', secs);

        console.log(output);
    }

    stop = function () {
        clearInterval(this.timer);
    };

    start = function () {
        this.render();
        this.timer = setInterval(this.render.bind(this), 1000);
    };
}

var clock = new Clock({
    template: 'h:m:s'
});

clock.start();
